import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

/**
 * RSS Feed which store podcast information such as title and description and episodes.
 * @author Raksit Mantanacharu 5710546402
 * @author Supanat Pokturng 5710546429
 * @version 2015.05.07
 *
 */
public class Feed {
	
	private List<SyndEntry> episodes;
	private SyndFeed feed;
	private String urlname;
	
	public Feed(String urlname) {
		episodes = new ArrayList<SyndEntry>();
		this.urlname = urlname;
		getInformation();
	}
	
	/** Connecting to Feed URL to receive episodes. */
	public void getInformation() {
		
		System.out.println(System.currentTimeMillis()*1.0/1000);
		try {
			URL url = new URL(urlname);
			try {
				HttpURLConnection httpcon = (HttpURLConnection)url.openConnection();
				SyndFeedInput input = new SyndFeedInput();
				try {
					feed = input.build(new XmlReader(httpcon));
					List<SyndEntry> entries = feed.getEntries();
					Iterator<SyndEntry> itEntries = entries.iterator();
					while (itEntries.hasNext()) {
						episodes.add(itEntries.next());
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (FeedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		System.out.println(System.currentTimeMillis()*1.0/1000);
	}
	
	public String getTitle() {
		return feed.getTitle();
	}
	
	public String getDescription() {
		return feed.getDescription();
	}
	
	public List<SyndEntry> getEpisodes() {
		return episodes;
	}
}
