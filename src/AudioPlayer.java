import java.util.ArrayList;
import java.util.List;

import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import javax.swing.JFrame;

/**
 * Audio Player which can play either single or playlist.
 * @author Raksit Mantanacharu 5710546402
 * @author Supanat Pokturng 5710546429
 * @version 2015.05.07
 */
public class AudioPlayer {

	private List <String> audioURL;
	private List<MediaPlayer> playlist = new ArrayList<MediaPlayer>();
	MediaPlayer player;
	int count = 0;
	JFrame frame;

	public AudioPlayer(List <String> audioURL) {
		this.audioURL = audioURL;
		initComponents();
	}

	/** This method is invoked on the EDT thread. */
	private void initComponents() {
		Platform.setImplicitExit(false);
		frame = new JFrame("Music Player");
		final JFXPanel fxPanel = new JFXPanel();
		frame.add(fxPanel);
		frame.setSize(500, 200);
		frame.setVisible(true);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				frame.getContentPane().removeAll();
				fxPanel.setVisible(false);
			}
		});

		Platform.runLater(new Runnable() {
			public void run() {
				initFX(fxPanel);
			}
		});
	}

	/** This method is invoked on the JavaFX thread. */
	private void initFX(JFXPanel fxPanel) {
		Scene scene = createScene();
		fxPanel.setScene(scene);
	}

	/** Create components on JavaFx Panel. */
	public Scene createScene() {
		Group root = new Group();
		Scene scene = new Scene(root, 500, 200);

		// Play song according to a playlist.
		for(int i = 0; i < audioURL.size(); i++) {
			playlist.add(new MediaPlayer(new Media(audioURL.get(i))));
		}

		player = playlist.get(count);
		// Adding delay between each track..
		player.setOnEndOfMedia(new Runnable() {
			public void run() {
				count++;
				if(count >= playlist.size()) return;
				SequentialTransition seqTransition = new SequentialTransition (
						new PauseTransition(Duration.millis(5000)));
				seqTransition.play();
				seqTransition.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						frame.getContentPane().removeAll();
						final JFXPanel fxPanel = new JFXPanel();
						frame.add(fxPanel);
						initFX(fxPanel);
					}
				});
			}
		});

		MediaControl mediaControl = new MediaControl(player);
		scene.setRoot(mediaControl);
		return scene;
	}
}
