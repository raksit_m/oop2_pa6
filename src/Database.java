import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class Database {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://158.108.139.164:3306/testdb";

	//  Database credentials
	static final String USER = "test";
	static final String PASS = "test";
	
	public static Connection getConnection(){
		Connection conn = null;
		Statement stmt = null;

		try{
			//STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			
			//STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = (Connection)DriverManager.getConnection(DB_URL,USER,PASS);
			//STEP 4: Execute a query
			System.out.println("Creating statement...");
			
		}catch(Exception e){ e.printStackTrace();};
		
		return conn;
	}
	
	public static List<String> getChannelSubscribed(String MACAddress) {
		List<String> list = new ArrayList<String>();
		Connection conn =  getConnection();
		String sqlCommand = String.format("SELECT channel FROM tbl_channel_subscribes WHERE macAddress='%s'",MACAddress);
		try {
			Statement statement = (Statement)conn.createStatement();
			ResultSet rs = statement.executeQuery(sqlCommand);
			while(rs.next()){
				String channel = rs.getString("channel");
				list.add(channel);
			}
			rs.close();

			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static List<String> getSubscribeOfChannel( String linkChannel ) {
		List<String> list = new ArrayList<String>();
		boolean getIn = false;
		Connection conn = getConnection();
		String sqlCommand = String.format("SELECT macAddress FROM tbl_channel_subscribes WHERE channel='%s'",linkChannel);
		try {
			Statement statement = (Statement)conn.createStatement();
			ResultSet rs = statement.executeQuery(sqlCommand);
			while(rs.next()) {
				getIn = true;
				String macAddress = rs.getString("macAddress");
				list.add(macAddress);
			}
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(getIn)
			return list;
		return null;
	}
	
	public static Map<String,List<String>> getAllSubscribeOfChannel( List<String> linkChannelList ) {
		Map<String,List<String>> map = new HashMap<String,List<String>>();
		boolean getIn = false;
		Connection conn = getConnection();
		String linkChannel;
		
		for( int i=0 ; i<linkChannelList.size() ; i++ ) {
			List<String> list = new ArrayList<String>();
			linkChannel = linkChannelList.get(i);
			String sqlCommand = String.format("SELECT macAddress FROM tbl_channel_subscribes WHERE channel='%s'",linkChannel);
			try {
				Statement statement = (Statement)conn.createStatement();
				ResultSet rs = statement.executeQuery(sqlCommand);
				while(rs.next()) {
					getIn = true;
					String macAddress = rs.getString("macAddress");
					System.out.println("linkChannel : "+ linkChannel);
					System.out.println("macAddress : " + macAddress);
					list.add(macAddress);
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(getIn)
				map.put(linkChannel, list);
		}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return map;
	}
	
	public static List<String> getAllChannel() {
		List<String> list = new ArrayList<String>();
		Connection conn = getConnection();
		String sqlCommand = String.format("SELECT * FROM tbl_all_channel");
		try {
			Statement statement = (Statement)conn.createStatement();
			ResultSet rs = statement.executeQuery(sqlCommand);
			while(rs.next()) {
				String linkChannel = rs.getString("link_channel");
				list.add(linkChannel);
			}
			rs.close();
			 conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static List<String> getCurrentChannelList(String macAddress) {
		List<String> list = new ArrayList<String>();
		Connection conn = getConnection();
		String sqlCommand = String.format("SELECT link_channel FROM tbl_new_channel WHERE macAddress = '%s'",macAddress);
		try {
			Statement statement = (Statement)conn.createStatement();
			ResultSet rs = statement.executeQuery(sqlCommand);
			while(rs.next()) {
				String linkChannel = rs.getString("link_channel");
				list.add(linkChannel);
			}
			rs.close();
			 conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/** Insert subscriber into a Database. */
	public static void insertSubscriber(String linkChannel, String MACAddress){
		Connection conn = getConnection();
		String sqlCommand =  String.format("INSERT INTO tbl_login VALUES ('%s','%s')", linkChannel, MACAddress);
		try {
			Statement statement =  (Statement) conn.createStatement();
			statement.executeUpdate(sqlCommand);
			statement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void insertCurrentAllChannel(String linkChannel , String MACAddress) {
		Connection conn = getConnection();
		String sqlCommand =  String.format("INSERT INTO tbl_new_channel VALUES ('%s','%s')", linkChannel, MACAddress);
		try {
			Statement statement =  (Statement) conn.createStatement();
			statement.executeUpdate(sqlCommand);
			statement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}