import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.sun.syndication.feed.synd.SyndEnclosure;

/**
 * Podcast Application GUI which can play audio and more. 
 * @author Raksit Mantanacharu 5710546402
 * @author Supanat Pokturng 5710546429
 * @version 2015.05.07
 */
public class MyPodcastGUI extends JFrame {

	/** Buttons and header. */
	private JLabel label_iconMyPodcast , label_iconMyPlaylist , label_iconWhatsNew , label_iconCharts;
	private JLabel label_headChannelList , label_headSubscribeList , label_headInformation;
	private JLabel label_top_headChannelList , label_top_headSubscribeList , label_top_headInformation;
	private JLabel label_headPlaylist , label_headInfoPlaylist , label_headSongList;
	private JLabel label_headNewChannel , label_headInfoNewChannel , label_headNewChannelSub;
	
	/** Playlist and What's New list. */
	private String[] playList;
	private String[] newChannelList; 
	
	/** List of channels, episodes, playlist and top chart list. */
	private JList list_channel , list_subscribe , list_playlist , list_song , list_top_Channel , list_top_subscribe , list_newChannel , list_newChannelSub;
	private JList list_episode;
	
	/** Scroll bar (used in list). */
	private JScrollPane scrollPaneChannelList , scrollPaneSubscribeList , scrollPaneSongList , scrollPanePlayList;
	private JScrollPane scrollPane_top_ChannelList , scrollPane_top_subscribeList , scrollPane_newChannel , scrollPane_newChannelSub;
	private JScrollPane scrollPane_episode;
	
	/** Storing data (title and description) which Database sent. */
	private String[][] data_description , data_top_description , data_new_description , data_infoPlaylist;
	private Map<String,String[]> data_new_subscribe , data_playlist;
	private Map<String, String[]> map_episodes;
	private List<String> channelList , linkChannelList , allLinkChannelList , nameTopChannelList;
	private List<Object> topChannelList;
	
	protected static Map<String,List<String>> map_subscribeList;
	private DefaultListModel model_list_channel , model_list_subscribe , model_list_topChannel , model_list_channelPlaylist , model_list_playlist;
	private DefaultListModel model_list_newChannel , model_list_topSubscribe , model_list_newChannelSub;
	private Map<String,Integer> map_sortChannel , map_topChart;
	private Map<String,String> map_allChannelDescription;
	private BackgroundUpdate backgroundUpdate;
	private Map<String,JList> map_jList;
	private List<Feed> feedList;
//	private LoadFeed2 loadFeed2;
	private Map<String,String[]> map_feedDescription;
	private List<String> currentChannelList , newLinkChannelList;
	
	/** Text area for showing description. */
	private JTextArea txtarea_description , txtarea_infoPlaylist , txtarea_top_description , txtarea_new_description;
	
	private AudioPlayer player;
	
	private int indexSelectedChannel;
	
	private String MACAddress;
	
	public MyPodcastGUI() {
		initComponents();
	}

	public void initComponents() {
		setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE);
		setTitle( "Podcast" );
		final Container container = super.getContentPane();
		container.setLayout( new BoxLayout( container , BoxLayout.X_AXIS) );
		container.setBackground(new Color(28, 27, 32));

		final JPanel pane_menu = new JPanel();
		pane_menu.setLayout( new BoxLayout( pane_menu , BoxLayout.Y_AXIS) );

		final JPanel pane_ChannelList = new JPanel();
		pane_ChannelList.setLayout( new BoxLayout( pane_ChannelList , BoxLayout.Y_AXIS) );
		pane_ChannelList.setBackground(new Color(255, 145, 0));
		pane_ChannelList.setPreferredSize( new Dimension(200,200) );
		final JPanel pane_info = new JPanel();
		pane_info.setLayout( new BorderLayout() );
		pane_info.setBackground(new Color(28, 27, 32));
		final JPanel pane_subscribesList = new JPanel();
		pane_subscribesList.setLayout( new BoxLayout( pane_subscribesList , BoxLayout.Y_AXIS) );
		pane_subscribesList.setBackground(new Color(255, 145, 0));
		
		final JPanel pane_playlist = new JPanel();
		pane_playlist.setLayout( new BoxLayout( pane_playlist , BoxLayout.Y_AXIS) );
		pane_playlist.setBackground(new Color(255, 145, 0));
		final JPanel pane_songList = new JPanel();
		pane_songList.setLayout( new BoxLayout( pane_songList , BoxLayout.Y_AXIS) );
		pane_songList.setBackground(new Color(255, 145, 0));
		final JPanel pane_infoPlaylist = new JPanel();
		pane_infoPlaylist.setLayout( new BorderLayout() );
		pane_infoPlaylist.setBackground(new Color(28, 27, 32));

		final JPanel pane_newChannel = new JPanel();
		pane_newChannel.setLayout( new BoxLayout( pane_newChannel , BoxLayout.Y_AXIS) );
		pane_newChannel.setBackground(new Color(255, 145, 0));
		final JPanel pane_infoNewChannel = new JPanel();
		pane_infoNewChannel.setLayout( new BorderLayout() );
		pane_infoNewChannel.setBackground(new Color(28, 27, 32));
		final JPanel pane_newChannelsub = new JPanel();
		pane_newChannelsub.setLayout( new BoxLayout( pane_newChannelsub , BoxLayout.Y_AXIS) );
		pane_newChannelsub.setBackground(new Color(255, 145, 0));

		final JPanel pane_top_ChannelList = new JPanel();
		pane_top_ChannelList.setLayout( new BoxLayout( pane_top_ChannelList , BoxLayout.Y_AXIS) );
		pane_top_ChannelList.setBackground(new Color(255, 145, 0));
		final JPanel pane_top_info = new JPanel();
		pane_top_info.setLayout( new BorderLayout() );
		pane_top_info.setBackground(new Color(28, 27, 32));
		final JPanel pane_top_subscribesList = new JPanel();
		pane_top_subscribesList.setLayout( new BoxLayout( pane_top_subscribesList , BoxLayout.Y_AXIS) );
		pane_top_subscribesList.setBackground(new Color(255, 145, 0));

		String [] arr = {""};

		label_iconMyPodcast = new JLabel(getImageIcon("images/mypodcast_pressed.png"));
		label_iconMyPlaylist = new JLabel( getImageIcon("images/myplaylist.png"));
		label_iconWhatsNew = new JLabel( getImageIcon("images/whatsnew.png"));
		label_iconCharts = new JLabel( getImageIcon("images/chart.png"));

		label_headChannelList = new JLabel("Channels",SwingConstants.CENTER);
		label_headChannelList.setForeground(new Color(28, 27, 32));
		label_headChannelList.setBackground(new Color(255, 145, 0));
		label_headChannelList.setOpaque(true);
		label_headChannelList.setFont( new Font(label_headChannelList.getFont().toString(),Font.PLAIN,20));
		
		model_list_channel = new DefaultListModel<String>();
		list_channel = new JList(model_list_channel);
		list_channel.setBackground(new Color(28, 27, 32));
		list_channel.setForeground(new Color(255, 145, 0));
		list_channel.setFixedCellWidth(200);
		list_channel.setCellRenderer( getRenderer() );

		list_channel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting())
					return;
				String channel = list_channel.getSelectedValue().toString();
				indexSelectedChannel = list_channel.getSelectedIndex();
				String linkChannel = linkChannelList.get(indexSelectedChannel);
//				String[] newList = data_subscribe.get(Channel);
//				list_subscribe.setListData(newList);
				
				Object[] newList = map_subscribeList.get(linkChannel).toArray();
				list_subscribe.setListData(newList);
				int number = map_subscribeList.get(linkChannel).size();
				String head_subscribe = String.format("Subscribers (%d)", number);
				label_headSubscribeList.setText(head_subscribe);
				label_headInformation.setText(channel);
				txtarea_description.setText(data_description[indexSelectedChannel][1]);
				String[] newListEpisode = map_episodes.get(channel);
				list_episode.setListData(newListEpisode);
			}
		});

		scrollPaneChannelList = new JScrollPane(list_channel);
		scrollPaneChannelList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		list_episode = new JList(arr);
		list_episode.setBackground(new Color(28, 27, 32));
		list_episode.setForeground(new Color(255, 145, 0));
		list_episode.setFixedCellWidth(200);
		list_episode.setCellRenderer( getRenderer() );
		list_episode.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if(evt.getClickCount() == 2) {
					List<String> audioURL = new ArrayList<String>();
					List<SyndEnclosure> encls = new Feed(linkChannelList.get(list_channel.getSelectedIndex())).getEpisodes().get(list_episode.getSelectedIndex()).getEnclosures();
					if(!encls.isEmpty()){
						for(SyndEnclosure encl : encls){
							audioURL.add(encl.getUrl().toString());
						}                       
					}
					if(player != null) player = null;
					player = new AudioPlayer(audioURL);
				}
			}
		});

		scrollPane_episode = new JScrollPane(list_episode);
		scrollPane_episode.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_headInformation = new JLabel("                                 ");
		label_headInformation.setForeground(new Color(255, 145, 0));
		label_headInformation.setBackground(Color.BLACK);
		label_headInformation.setSize(200,100);
		label_headInformation.setOpaque(true);
		label_headInformation.setFont( new Font(label_headInformation.getFont().toString(),Font.PLAIN,20));
		label_headInformation.setPreferredSize( new Dimension(300,100));
		label_headInformation.setHorizontalAlignment(SwingConstants.CENTER);

		txtarea_description = new JTextArea(15,15);
		txtarea_description.setBackground(new Color(28, 27, 32));
		//txtarea_description.setForeground(new Color(255, 145, 0));
		txtarea_description.setForeground( Color.white);
		txtarea_description.setEditable(false);
		txtarea_description.setWrapStyleWord(true);
		txtarea_description.setLineWrap(true);
		txtarea_description.setPreferredSize( new Dimension(200,200));

		label_headSubscribeList = new JLabel("Subscribers");
		label_headSubscribeList.setBackground(new Color(255, 145, 0));
		label_headSubscribeList.setForeground(new Color(28, 27, 32));
		label_headSubscribeList.setOpaque(true);
		label_headSubscribeList.setFont( new Font(label_headSubscribeList.getFont().toString(),Font.PLAIN,20));
		label_headSubscribeList.setHorizontalAlignment(SwingConstants.CENTER);
		
		model_list_subscribe = new DefaultListModel<String>();
		list_subscribe = new JList(model_list_subscribe);
		list_subscribe.setBackground(new Color(28, 27, 32));
		list_subscribe.setForeground(new Color(255, 145, 0));
		list_subscribe.setFixedCellWidth(200);
		list_subscribe.setCellRenderer( getRenderer() );
		scrollPaneSubscribeList = new JScrollPane(list_subscribe);
		scrollPaneSubscribeList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_headPlaylist = new JLabel("Playlist",SwingConstants.CENTER);
		label_headPlaylist.setForeground(new Color(28, 27, 32));
		label_headPlaylist.setBackground(new Color(255, 145, 0));
		label_headPlaylist.setOpaque(true);
		label_headPlaylist.setFont( new Font(label_headPlaylist.getFont().toString(),Font.PLAIN,20));
		
		model_list_channelPlaylist = new DefaultListModel();
		list_playlist = new JList(model_list_channelPlaylist);
		list_playlist.setBackground(new Color(28, 27, 32));
		list_playlist.setForeground(new Color(255, 145, 0));
		list_playlist.setFixedCellWidth(200);
		list_playlist.setCellRenderer( getRenderer()  );
		list_playlist.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting())
					return;
				String playlistName = list_playlist.getSelectedValue().toString();
				int index = list_playlist.getSelectedIndex();
				String[] newList = data_playlist.get(playlistName);
				list_song.setListData(newList);

				label_headInfoPlaylist.setText(playlistName);
				txtarea_infoPlaylist.setText(data_infoPlaylist[index][1]);

			}
		});
		scrollPanePlayList = new JScrollPane(list_playlist);
		scrollPanePlayList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_headInfoPlaylist = new JLabel("                                 ");
		label_headInfoPlaylist.setForeground(new Color(255, 145, 0));
		label_headInfoPlaylist.setBackground(Color.BLACK);
		label_headInfoPlaylist.setSize(200,100);
		label_headInfoPlaylist.setOpaque(true);
		label_headInfoPlaylist.setFont( new Font(label_headInfoPlaylist.getFont().toString(),Font.PLAIN,20));
		label_headInfoPlaylist.setPreferredSize( new Dimension(200,100));
		label_headInfoPlaylist.setHorizontalAlignment(SwingConstants.CENTER);

		txtarea_infoPlaylist = new JTextArea(15,15);
		txtarea_infoPlaylist.setBackground(new Color(28, 27, 32));
		txtarea_infoPlaylist.setForeground(new Color(255, 145, 0));
		txtarea_infoPlaylist.setEditable(false);
		txtarea_infoPlaylist.setWrapStyleWord(true);
		txtarea_infoPlaylist.setLineWrap(true);
		txtarea_infoPlaylist.setPreferredSize( new Dimension(200,200));

		label_headSongList = new JLabel("Song list" , SwingConstants.CENTER);
		label_headSongList.setForeground(new Color(28, 27, 32));
		label_headSongList.setBackground(new Color(255, 145, 0));
		label_headSongList.setOpaque(true);
		label_headSongList.setFont( new Font(label_headSongList.getFont().toString(),Font.PLAIN,20));
		
		model_list_playlist = new DefaultListModel();
		list_song = new JList(model_list_playlist);
		list_song.setBackground(new Color(28, 27, 32));
		list_song.setForeground(new Color(255, 145, 0));
		list_song.setFixedCellWidth(200);
		list_song.setCellRenderer( getRenderer() );
		scrollPaneSongList = new JScrollPane(list_song);
		scrollPaneSongList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		//NEW ChannelLLLLLLLLLLLLLLLLLLLLLL

		label_headNewChannel = new JLabel("New Channels",SwingConstants.CENTER);
		label_headNewChannel.setForeground(new Color(28, 27, 32));
		label_headNewChannel.setBackground(new Color(255, 145, 0));
		label_headNewChannel.setOpaque(true);
		label_headNewChannel.setFont( new Font(label_headNewChannel.getFont().toString(),Font.PLAIN,20));

		model_list_newChannel = new DefaultListModel();
		list_newChannel = new JList(model_list_newChannel);
		list_newChannel.setBackground(new Color(28, 27, 32));
		list_newChannel.setForeground(new Color(255, 145, 0));
		list_newChannel.setFixedCellWidth(200);
		list_newChannel.setCellRenderer( getRenderer() );

		list_newChannel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting())
					return;
				String channel = list_newChannel.getSelectedValue().toString();
				int index = list_newChannel.getSelectedIndex();
				String link = newLinkChannelList.get(index);
				Object[] newList = map_subscribeList.get(link).toArray();
				list_newChannelSub.setListData(newList);

				label_headInfoNewChannel.setText(channel);
				txtarea_new_description.setText(map_allChannelDescription.get(channel));
			}
		});

		scrollPane_newChannel = new JScrollPane(list_newChannel);
		scrollPane_newChannel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_headInfoNewChannel = new JLabel("                                 ");
		label_headInfoNewChannel.setForeground(new Color(255, 145, 0));
		label_headInfoNewChannel.setBackground(Color.BLACK);
		label_headInfoNewChannel.setSize(200,100);
		label_headInfoNewChannel.setOpaque(true);
		label_headInfoNewChannel.setFont( new Font(label_headInfoNewChannel.getFont().toString(),Font.PLAIN,20));
		label_headInfoNewChannel.setPreferredSize( new Dimension(200,100));
		label_headInfoNewChannel.setHorizontalAlignment(SwingConstants.CENTER);

		txtarea_new_description = new JTextArea(15,15);
		txtarea_new_description.setBackground(new Color(28, 27, 32));
		txtarea_new_description.setForeground(new Color(255, 145, 0));
		txtarea_new_description.setEditable(false);
		txtarea_new_description.setWrapStyleWord(true);
		txtarea_new_description.setLineWrap(true);
		txtarea_new_description.setPreferredSize( new Dimension(200,200));

		label_headNewChannelSub = new JLabel("Subscribers");
		label_headNewChannelSub.setBackground(new Color(255, 145, 0));
		label_headNewChannelSub.setForeground(new Color(28, 27, 32));
		label_headNewChannelSub.setOpaque(true);
		label_headNewChannelSub.setFont( new Font(label_headNewChannelSub.getFont().toString(),Font.PLAIN,20));
		label_headNewChannelSub.setHorizontalAlignment(SwingConstants.CENTER);
		
		model_list_newChannelSub = new DefaultListModel();
		list_newChannelSub = new JList(model_list_newChannelSub);
		list_newChannelSub.setBackground(new Color(28, 27, 32));
		list_newChannelSub.setForeground(new Color(255, 145, 0));
		list_newChannelSub.setFixedCellWidth(200);
		list_newChannelSub.setCellRenderer( getRenderer() );
		scrollPane_newChannelSub = new JScrollPane(list_newChannelSub);
		scrollPane_newChannelSub.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		//CHARTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT

		label_top_headChannelList = new JLabel("Top Charts",SwingConstants.CENTER);
		label_top_headChannelList.setForeground(new Color(28, 27, 32));
		label_top_headChannelList.setBackground(new Color(255, 145, 0));
		label_top_headChannelList.setOpaque(true);
		label_top_headChannelList.setFont( new Font(label_top_headChannelList.getFont().toString(),Font.PLAIN,20));
		
		model_list_topChannel = new DefaultListModel();
		list_top_Channel = new JList(model_list_topChannel);
		list_top_Channel.setBackground(new Color(28, 27, 32));
		list_top_Channel.setForeground(new Color(255, 145, 0));
		list_top_Channel.setFixedCellWidth(200);
		list_top_Channel.setCellRenderer( getRenderer() );

		list_top_Channel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting())
					return;
				String topChannel = list_top_Channel.getSelectedValue().toString();
				int indexSelectedTopChannel = list_top_Channel.getSelectedIndex();
				String link = (String) topChannelList.get(indexSelectedTopChannel);
				Object[] newList = map_subscribeList.get(link).toArray();
				list_top_subscribe.setListData(newList);
				int number = map_subscribeList.get(link).size();
				String head_subscribe = String.format("Subscribers (%d)", number);
				label_top_headSubscribeList.setText(head_subscribe);

				label_top_headInformation.setText(topChannel);
				txtarea_top_description.setText(map_allChannelDescription.get(topChannel));
			}
		});

		scrollPane_top_ChannelList = new JScrollPane(list_top_Channel);
		scrollPane_top_ChannelList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_top_headInformation = new JLabel("                                 ");
		label_top_headInformation.setForeground(new Color(255, 145, 0));
		label_top_headInformation.setBackground(Color.BLACK);
		label_top_headInformation.setSize(200,100);
		label_top_headInformation.setOpaque(true);
		label_top_headInformation.setFont( new Font(label_top_headInformation.getFont().toString(),Font.PLAIN,20));
		label_top_headInformation.setPreferredSize( new Dimension(200,100));
		label_top_headInformation.setHorizontalAlignment(SwingConstants.CENTER);

		txtarea_top_description = new JTextArea(15,15);
		txtarea_top_description.setBackground(new Color(28, 27, 32));
		txtarea_top_description.setForeground(new Color(255, 145, 0));
		txtarea_top_description.setEditable(false);
		txtarea_top_description.setWrapStyleWord(true);
		txtarea_top_description.setLineWrap(true);
		txtarea_top_description.setPreferredSize( new Dimension(200,200));

		label_top_headSubscribeList = new JLabel("Subscribers");
		label_top_headSubscribeList.setBackground(new Color(255, 145, 0));
		label_top_headSubscribeList.setForeground(new Color(28, 27, 32));
		label_top_headSubscribeList.setOpaque(true);
		label_top_headSubscribeList.setFont( new Font(label_top_headSubscribeList.getFont().toString(),Font.PLAIN,20));
		label_top_headSubscribeList.setHorizontalAlignment(SwingConstants.CENTER);
		
		model_list_topSubscribe = new DefaultListModel();
		list_top_subscribe = new JList(model_list_topSubscribe);
		list_top_subscribe.setBackground(new Color(28, 27, 32));
		list_top_subscribe.setForeground(new Color(255, 145, 0));
		list_top_subscribe.setFixedCellWidth(200);
		list_top_subscribe.setCellRenderer( getRenderer() );
		scrollPane_top_subscribeList = new JScrollPane(list_top_subscribe);
		scrollPane_top_subscribeList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label_iconMyPodcast.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				container.removeAll();
				container.revalidate();
				container.repaint();
				label_iconMyPodcast.setIcon(getImageIcon("images/mypodcast_pressed.png"));
				label_iconMyPlaylist.setIcon(getImageIcon("images/myplaylist.png"));
				label_iconCharts.setIcon(getImageIcon("images/chart.png"));
				label_iconWhatsNew.setIcon(getImageIcon("images/whatsnew.png"));
				container.add(pane_menu);
				container.add(pane_ChannelList);
				container.add(pane_info);
				container.add(pane_subscribesList);
			}
		});

		label_iconMyPlaylist.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				container.removeAll();
				container.revalidate();
				container.repaint();
				label_iconMyPodcast.setIcon(getImageIcon("images/mypodcast.png"));
				label_iconMyPlaylist.setIcon(getImageIcon("images/myplaylist_pressed.png"));
				label_iconCharts.setIcon(getImageIcon("images/chart.png"));
				label_iconWhatsNew.setIcon(getImageIcon("images/whatsnew.png"));
				container.add(pane_menu);
				container.add(pane_playlist);
				container.add(pane_infoPlaylist);
				container.add(pane_songList);

			}
		});

		label_iconWhatsNew.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				container.removeAll();
				container.revalidate();
				container.repaint();
				label_iconMyPodcast.setIcon(getImageIcon("images/mypodcast.png"));
				label_iconMyPlaylist.setIcon(getImageIcon("images/myplaylist.png"));
				label_iconCharts.setIcon(getImageIcon("images/chart.png"));
				label_iconWhatsNew.setIcon(getImageIcon("images/whatsnew_pressed.png"));
				container.add(pane_menu);
				container.add(pane_newChannel);
				container.add(pane_infoNewChannel);
				container.add(pane_newChannelsub);

			}
		});

		label_iconCharts.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				container.removeAll();
				container.revalidate();
				container.repaint();
				label_iconMyPodcast.setIcon(getImageIcon("images/mypodcast.png"));
				label_iconMyPlaylist.setIcon(getImageIcon("images/myplaylist.png"));
				label_iconCharts.setIcon(getImageIcon("images/chart_pressed.png"));
				label_iconWhatsNew.setIcon(getImageIcon("images/whatsnew.png"));
				container.add(pane_menu);
				container.add(pane_top_ChannelList);
				container.add(pane_top_info);
				container.add(pane_top_subscribesList);
			}
		});

		pane_menu.add(label_iconMyPodcast);
		pane_menu.add(label_iconMyPlaylist);
		pane_menu.add(label_iconWhatsNew);
		pane_menu.add(label_iconCharts);

		JPanel tempPane = new JPanel();
		tempPane.setLayout( new BoxLayout( tempPane , BoxLayout.Y_AXIS));
		tempPane.add(label_headInformation);
		tempPane.add(txtarea_description);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
		buttonPane.setBackground(new Color(28, 27, 32));
		
		JLabel btnPlay = new JLabel(getImageIcon("images/play_button.png"));
		btnPlay.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				List<String> audioURL = new ArrayList<String>();
				List<SyndEnclosure> encls = new Feed(linkChannelList.get(list_channel.getSelectedIndex())).getEpisodes().get(list_episode.getSelectedIndex()).getEnclosures();
				if(!encls.isEmpty()){
					for(SyndEnclosure encl : encls){
						audioURL.add(encl.getUrl().toString());
					}                       
				}
				if(player != null) player = null;
				player = new AudioPlayer(audioURL);
			}
		});
		
		JLabel btnSubscribe = new JLabel(getImageIcon("images/subscribe_button.png"));
		btnSubscribe.addMouseListener( new MouseAdapter() {
			public void mouseClicked( MouseEvent e ) {
				String linkChannel = linkChannelList.get(indexSelectedChannel);
				System.out.println(linkChannel +" : "+MACAddress);
				Database.insertSubscriber(linkChannel, MACAddress);
			}
		});
		
		JPanel paneSubscribe = new JPanel();
		paneSubscribe.setLayout( new BorderLayout());
		paneSubscribe.setBackground(new Color(28, 27, 32));
		JPanel panePlay = new JPanel();
		panePlay.setLayout( new BorderLayout());
		panePlay.setBackground(new Color(28, 27, 32));
		paneSubscribe.add(btnSubscribe , BorderLayout.CENTER);
		panePlay.add(btnPlay , BorderLayout.CENTER);
		
		buttonPane.add(btnPlay);
		buttonPane.add(btnSubscribe);
		tempPane.add(panePlay);
		tempPane.setAlignmentX(CENTER_ALIGNMENT);
		tempPane.setBackground(new Color(28, 27, 32));
		
		paneSubscribe.add(btnSubscribe , BorderLayout.CENTER);
		panePlay.add(btnPlay , BorderLayout.CENTER);
		
		pane_info.add(tempPane , BorderLayout.BEFORE_FIRST_LINE);
		pane_info.add(scrollPane_episode , BorderLayout.CENTER);
		pane_ChannelList.add(label_headChannelList);
		pane_ChannelList.add(scrollPaneChannelList);
		pane_subscribesList.add(label_headSubscribeList);
		pane_subscribesList.add(scrollPaneSubscribeList);

		pane_playlist.add(label_headPlaylist);
		pane_playlist.add(scrollPanePlayList);
		pane_infoPlaylist.add(label_headInfoPlaylist , BorderLayout.BEFORE_FIRST_LINE);
		pane_infoPlaylist.add(txtarea_infoPlaylist , BorderLayout.SOUTH);
		pane_songList.add(label_headSongList);
		pane_songList.add(scrollPaneSongList);

		pane_newChannel.add(label_headNewChannel);
		pane_newChannel.add(scrollPane_newChannel);
		pane_infoNewChannel.add(label_headInfoNewChannel , BorderLayout.BEFORE_FIRST_LINE);
		pane_infoNewChannel.add(txtarea_new_description , BorderLayout.CENTER);
		pane_infoNewChannel.add(paneSubscribe , BorderLayout.SOUTH);
		pane_newChannelsub.add(label_headNewChannelSub);
		pane_newChannelsub.add(scrollPane_newChannelSub);

		pane_top_info.add(label_top_headInformation , BorderLayout.BEFORE_FIRST_LINE);
		pane_top_info.add(txtarea_top_description , BorderLayout.CENTER);
		pane_top_info.add(paneSubscribe, BorderLayout.SOUTH);
		pane_top_ChannelList.add(label_top_headChannelList);
		pane_top_ChannelList.add(scrollPane_top_ChannelList);
		pane_top_subscribesList.add(label_top_headSubscribeList);
		pane_top_subscribesList.add(scrollPane_top_subscribeList);


		container.add(pane_menu);
		container.add(pane_ChannelList);
		container.add(pane_info);
		container.add(pane_subscribesList);

		this.pack();
		super.setVisible(true);
		
		createData();
	}

	private ImageIcon getImageIcon( String dir ) {
		ClassLoader loader = this.getClass().getClassLoader();
		ImageIcon imgIcon = new ImageIcon(loader.getResource(dir));
		return imgIcon;
	}

	public static void main(String [] args) {
		MyPodcastGUI gui = new MyPodcastGUI();
	}

	private ListCellRenderer<? super String> getRenderer() {
		return new DefaultListCellRenderer(){
			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				JLabel listCellRendererComponent = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected,cellHasFocus);
				listCellRendererComponent.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0,new Color(255, 145, 0)));
				return listCellRendererComponent;
			}
		};
	}

	private void createData() {
		
		MACAddress = getMACAddress();
		linkChannelList = new ArrayList<String>();
		channelList = new ArrayList<String>();
		map_subscribeList = new HashMap<String,List<String>>();
		allLinkChannelList = new ArrayList<String>();
		allLinkChannelList = Database.getAllChannel();
		map_allChannelDescription = new HashMap<String,String>();
		nameTopChannelList = new ArrayList<String>();
		feedList = new ArrayList<Feed>();
		map_episodes = new HashMap<String,String[]>();
		map_sortChannel = new HashMap<String,Integer>();
		map_topChart = new HashMap<String,Integer>();
		topChannelList = new ArrayList<Object>();
		currentChannelList = new ArrayList<String>();
		map_feedDescription = new HashMap<String,String[]>();
		map_subscribeList = Database.getAllSubscribeOfChannel(allLinkChannelList);
		newLinkChannelList = new ArrayList<String>();
		
		Runnable task1 = new Task1();
		Runnable task2 = new Task2();
		Runnable task3 = new Task3();
		Runnable task4 = new Task4();
		Runnable task5 = new Task5();
		Thread th1 = new Thread(task1);
		Thread th2 = new Thread(task2);
		Thread th3 = new Thread(task3);
		Thread th4 = new Thread(task4);
		Thread th5 = new Thread(task5);
		
		try {
			th1.start();
			th1.join();
			th2.start();
			th2.join();
			th3.start();
			th3.join();
			th4.start();
			th4.join();
			th5.start();
			th5.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static String getMACAddress() {
		String strMAC = "127.0.0.1";
		try {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			strMAC = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strMAC;
	}
	
	private static HashMap sortByValues(HashMap map) { 
	       List list = new LinkedList(map.entrySet());
	       // Defined Custom Comparator here
	       Collections.sort(list, new Comparator() {
	            public int compare(Object o1, Object o2) {
	               return ((Comparable) ((Map.Entry) (o1)).getValue())
	                  .compareTo(((Map.Entry) (o2)).getValue());
	            }
	       });

	       // Here I am copying the sorted list in HashMap
	       // using LinkedHashMap to preserve the insertion order
	       HashMap sortedHashMap = new LinkedHashMap();
	       for (Iterator it = list.iterator(); it.hasNext();) {
	              Map.Entry entry = (Map.Entry) it.next();
	              sortedHashMap.put(entry.getKey(), entry.getValue());
	       } 
	       return sortedHashMap;
	  }
	
	public Map<String,List<String>> getMapSubscribeList() {
		return this.map_subscribeList;
	}
	
	class LoadFeed extends SwingWorker<Void,Void>{
		private int i;
		public LoadFeed(int i){
			this.i = i;
		}
		@Override
		protected Void doInBackground() throws Exception {
			System.out.println(linkChannelList.get(i));
			Feed feed = new Feed(linkChannelList.get(i));
			model_list_channel.add(0, feed.getTitle());
			channelList.add(feed.getTitle());
			data_description[i][0] = feed.getTitle();
			data_description[i][1] = feed.getDescription();
			String[] arrEpisode = new String[feed.getEpisodes().size()];
			for(int j = 0; j < arrEpisode.length; j++) {
				arrEpisode[j] = feed.getEpisodes().get(j).getTitle();
			}
			map_episodes.put( data_description[i][0] , arrEpisode );
			return null;
		}
		
		protected void done(){
		}
	}

	class Task1 implements Runnable{
		@Override
		public void run() {
			linkChannelList = Database.getChannelSubscribed(MACAddress);
			
			data_description = new String[linkChannelList.size()][2];
			for( int i=0 ; i<linkChannelList.size() ; i++) {
				Feed feed = new Feed(linkChannelList.get(i));
				model_list_channel.addElement(feed.getTitle());
				channelList.add(feed.getTitle());
				data_description[i][0] = feed.getTitle();
				data_description[i][1] = feed.getDescription();
				String[] arrEpisode = new String[feed.getEpisodes().size()];
				for(int j = 0; j < arrEpisode.length; j++) {
					arrEpisode[j] = feed.getEpisodes().get(j).getTitle();
				}
				map_episodes.put( data_description[i][0] , arrEpisode );
			}
		}
	}
	
	class Task2 implements Runnable{
		@Override
		public void run() {
			Feed feed;
			String title;
			String description;
			String [] arr;
			for(int i = 0 ; i<allLinkChannelList.size() ; i++) {
				feed = new Feed(allLinkChannelList.get(i));
				title = feed.getTitle();
				description = feed.getDescription();
				arr = new String[]{title,description};
				map_feedDescription.put(allLinkChannelList.get(i),arr);
				map_allChannelDescription.put(title, description);
				feedList.add(feed);
			}
			
		}
	}
	
	class Task3 implements Runnable{
		@Override
		public void run() {
			List<String> list = new ArrayList<String>();
			for(String link : allLinkChannelList) {
				list = map_subscribeList.get(link);
				map_sortChannel.put(link, list.size());
			}
			map_topChart = sortByValues((HashMap)map_sortChannel);
			
			Object [] arr = map_topChart.keySet().toArray();
			for( int i=arr.length-1 ; i>=0 ; i-- ) {
				topChannelList.add(arr[i]);
			}
			
			data_top_description = new String[topChannelList.size()][2];
			for( int i=0 ; i<topChannelList.size() ; i++) {
				String name = map_feedDescription.get(topChannelList.get(i))[0];
				String des = map_feedDescription.get(topChannelList.get(i))[1];
				model_list_topChannel.addElement(name);
				data_top_description[i][0] = name;
				data_top_description[i][1] = des;
				nameTopChannelList.add(data_top_description[i][0]);
			}
		}
	}
	
	class Task4 implements Runnable{
		@Override
		public void run() {
			
			currentChannelList = Database.getCurrentChannelList(MACAddress);
			for(int i=0 ; i<allLinkChannelList.size() ; i++) {
				if(!currentChannelList.contains(allLinkChannelList.get(i))) {
					int size = allLinkChannelList.size() - currentChannelList.size();
					String link = allLinkChannelList.get(i);
					currentChannelList.add(link);
					Database.insertCurrentAllChannel( link , MACAddress);
					newChannelList = new String[size];
					newLinkChannelList.add(link);
					data_new_description = new String[size][2];
					//List<String> list_newSubscribe = new ArrayList<String>();
					System.out.println(map_feedDescription);
					model_list_newChannel.addElement(map_feedDescription.get(link)[0]);
									
					
				}
			}
//			data_new_description = new String[2][2];
//			data_new_description[0][0] = "poom and petjah";
//			data_new_description[0][1] = "Laugh only !!";
//			data_new_description[1][0] = "Fahsai";
//			data_new_description[1][1] = "Sexyy @#$%^&*(*&^%$";
//
//			newChannelList = new String[data_new_description.length];
//
//			for(int i=0 ; i<data_new_description.length ; i++ ) {
//				newChannelList[i] = data_new_description[i][0];
//			}
//
//			String [] temp10 = {"JAME"};
//			String [] temp11 = {};
		}
	}
	
	class Task5 implements Runnable{
		@Override
		public void run() {
			data_infoPlaylist = new String[2][2];
			data_infoPlaylist[0][0] = "EDM";
			data_infoPlaylist[0][1] = "Dancing and Dancing";
			data_infoPlaylist[1][0] = "Chill songs";
			data_infoPlaylist[1][1] = "Chill :)";

			playList = new String[data_infoPlaylist.length];

			for(int i=0 ; i<data_infoPlaylist.length ; i++) {
				playList[i] = data_infoPlaylist[i][0];
			}

			String [] song1 = {"LOKO MIX","Eclipse","Spaceman","FUN MIX"};
			String [] song2 = {"Star","Sticker","Event"};

			data_playlist = new HashMap<String , String[] >();
			data_playlist.put( playList[0] , song1 );
			data_playlist.put( playList[1] , song2 );
			
			map_jList = new HashMap<String,JList>();
			map_jList.put("channel",list_channel);
			map_jList.put("subscribe",list_subscribe);
			
			backgroundUpdate = new BackgroundUpdate(map_jList);
			Thread thread = new Thread(backgroundUpdate);
			thread.start();
		}
	}
}