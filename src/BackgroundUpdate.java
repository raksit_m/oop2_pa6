import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 * 
 * @author Raksit Mantanacharu 5710546402
 * @author Supanat Pokturng 5710546429
 * @version 2015.05.07
 * 
 */
public class BackgroundUpdate implements Runnable{
	
	private Map<String,JList> map_jList;
	BackgroundUpdate( Map<String,JList> map ) {
		this.map_jList = map;
	}
	
	public void run() {
		List<String> allChannelList = new ArrayList<String>();
		Map<String,List<String>> map_user = new HashMap<String,List<String>>();
		Map<String,List<String>> map_database = new HashMap<String,List<String>>();
		List<String> list_database = new ArrayList<String>();
		List<String> list_user = new ArrayList<String>();
		Object [] arrKey;
		while(true) {
			allChannelList = Database.getAllChannel();
			map_database = Database.getAllSubscribeOfChannel(allChannelList);
			map_user = MyPodcastGUI.map_subscribeList;
			arrKey = map_database.keySet().toArray();
			for( int i=0 ; i<map_database.size() ; i++) {
				list_database = map_database.get(arrKey[i].toString());
				list_user = map_user.get(arrKey[i].toString());
				if(list_database.size() != list_user.size()) {
					for(String str : list_database){
						if(!list_user.contains(str)) {
							list_user.add(str);
							System.out.println("MAP : " + map_jList);
							System.out.println("LIST : " + map_jList.get("subscribe"));
							Object [] arr = list_user.toArray();
							map_jList.get("subscribe").setListData(arr);
						}
					}
				}
			}
			try {
				Thread.sleep(6500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
