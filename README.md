# OOP2 PA6 - Podcast Application

### Members

1.) Raksit Mantanacharu 5710546402

2.)	Supanat Pokterng 5710546429

### Description

A podcast application for broadcasting the audio, such as 
music and documentaries,
with a graphical user interface.



### Features

-   Can create their own channels to add audio

-   Can subscribe and access to other channels

-   Can display featured and favourite channels

-   Uses Material design for GUI



### View

![My Channel.png](https://bitbucket.org/repo/9yLqyE/images/2462876628-My%20Channel.png)